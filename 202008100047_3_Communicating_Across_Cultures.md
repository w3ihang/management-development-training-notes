# 3 Communicating Across Cultures
> We make assumptions which may lead to biases
## 3.1 Understanding Other Cultures
### 3.1.1 Being attuned to other cultures
- Low-Context Cultures
	- Messages are spelled out
	- Communication is verbatim
	- More talking than silence
![](/source/images/low-context-cultures.png)
- High-Context Cultures
	- Messages are not explicit
	- Meanings must be inferred
	- More silence than talking
![](/source/images/high-context-cultures.png)
- To figure out the context of a culture is to collect data - observe
	- Personal space - do people speak standing close to one another - culture values relationships and face-time
	- Physical touch - hugging, holding hands, etc - culture values relationships and face-time
	- Physical animation
	- Work environment
### 3.1.2 Navigating language differences
- Observe the way the local language is spoken
	- Speech rates and silences
	- Avoid all sports idioms (sports metaphorrs)
	- Simplify
	- Flex your directness
- Adapt to match the local style
### 3.1.3 Time orientation in different cultures
> How time is viewed in your new environment
- Polychronic Time Cultures
	- Messages are communicated cyclically i.e. many things happen at once
- Monochronic Time Cultures
	- Messages are shared linearly
- Time Orientation Clues
	- Written communication
		- Block paragraphs vs bullet points (mono)
	- Presentations
		- With (mono) or without slides
	- Meeting start and finish times
	- Pace of life
## 3.2 Engaging Other Cultures
### 3.2.1 Building rapport across cultures
- Find common ground
- Do your homework
	- Geography, current news, politics, sports
- Avoid using a formal agenda
	- "Let's do an ice breaker" or "Let's get to know each other" is too spelt out in high-context cultures and people-focused cultures
- Leverage technology
	- Video calls and allow for small talks
### 3.2.2 Adapting communication style: direct and indirect
- Direct Communicators
	- Say what they mean, clearly
	- Share their opinions and feedback
	- Speak succintly
- Indirect Communicators
	- Gather information
	- Ask questions
	- Offer suggestions
	- Avoid confrontation
### 3.2.3 Adapting communication style: formal and informal
- Communicating Formally e.g. China, Japan
	- Provide context for meetings
	- Share an agenda in advance
	- List attendees' titles
	- Invite experienced attendees to lead
- Communicating Informally e.g. US, Australia
### 3.2.4 Adapting communication style: non-native English
- Non-native
	- Take notes
	- Ask questions and do not make assumptions help avoid miscomunications
- Native
	- Be empatetic with non-native speakers
	- Do not make assumptions
### 3.2.5 Enhancing cultural acuity
- Be informed about local cultures
- Practice the appropriate introductions
	- Doing cultures - focus on job title and profession
	- Being cultures - focus on non-profession
- Avoid assuming English is universal
- Use TV at the training tool
- Become comfortable with accents