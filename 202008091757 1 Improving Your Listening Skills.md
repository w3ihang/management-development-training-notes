# 1. Improving Your Listening Skills
## 1.1 Laying the Groundwork for Listening
### 1.1.1 Why do you have a hard time listening?
- We're distracted
- We're interrupting e.g. excited to contribute 
- We're nervous e.g. thinking what our responses will be when someone is talking
- We disagree
> When you're not listening, you alienate people
### 1.1.2 What are the signs you're not listening?
- Interrupting 
- Changing topics
- Debating (wait until the speaker is done)
- Playing with devices
- Asking them to repeat (frequently)
### 1.1.3 What to do when you're not being listened to
To become a better listener is to realise what it feels like you're not being listened well
- Notice how it makes you feel
- Try to get the attention back e.g. lower your voice to make it hard to hear you. silcences, pauses
- Say something but don't be accusatory e.g. is this not a good time blah
### 1.1.4 How to adopt the mindset of listening
- Understand your motivation and keep your reason in mind
- Show you care
- Understand the real problem or issue
- Embrace your curiousity e.g. gossip?
- Recognise the consequences when people feel not being heard
	- People stop trying
	- People resent you
	- People become less effective
### 1.1.5 How to set the stage for high-stakes conversations
- Identify the right time e.g. not a best moment if the other person is facing a huge deadline
- Pick the right setting i.e. venue
- Be ready to focus e.g. rest well and eat well before holding the conversation
- Consider involving others
## 1.2 Listening Well, Even When It's Hard
### 1.2.1 The structure of meaningful listening
Six cornerstone principles to follow to listen effectively
- Use silence and space
- Use eye contact @@ to show you're engaged
- Be mindful of body language e.g. don't fold your arms
- Use empathy e.g. I too have a failed project blah
- Ask open-ended questions
- Use the "help me understand" approach i.e. ask for more data
### 1.2.2 How to listen for what's not being said
- Emotion e.g. look for discrepancies - normally not emotional person being emotional and sensitive
- Subtext???
- Microexpression
- Tone of voice
- Eye contact
- Energy level
### 1.2.3 How to keep yourself from interrupting
- Take notes
- Focus on breathing
- Remember: You gain nothing from interrupting
### 1.2.4 How to let them know you've heard them
- Validate the other person
	- Assimilate, restate in your own words to show you understand the impact
- Pay attention to the words they use
- Ask them to expand on their point
- Ask them for possible solutions
### 1.2.5 What to do when you're listening to someone annoying
|Challenges|Technique|
|---|---|
|They may be passionate and you're not|Try to connect with their passion e.g. how do you get interested in this..|
|They repeat because they may not think you're listening|Restate their point with an action plan|
|They may be angry|If you're at fault, apologize; Otherwise, understand what they need|
|They may have a varying worldview|Listen and discover|