# 2 Communication within Teams
> TEAM - Together Everyone Achieves More

> Effective team work begins and ends with communication
## 2.1 Foundations of High-Performing Teams
### 2.1.1 Establish a team charter

- An important tool to
	- establish expectations
	- sets boundaries
	- helps your team stay focused
- A living document that
	- establishes team membership
		- who is on your team
		- what does each person bring
		- do you have the right people
	- defines purpose
		- what is your team's purpose
		- why have you come together
	- outlines working parameters [2.1.2, 2.1.3]
### 2.1.2 Define roles and commitments
- Define key team roles
	- Convener
		- determines whether to hold a meeting
		- creates meeting agenda
		- schedules a meeting
	- Recorder
		- takes detailed notes
		- provides a list of participants
		- notes down action items
		- sends meeting minutes to participants
	- Monitor
		- helps the group adhere to the agenda
- Detemine conditions of satisfaction
	- are the minimal requirements needed to reach completion of the project
### 2.1.3 Make decisions
- Decision-making styles
	- Commanding - team leader makes the decision
		- \+ saves time
		- \- alienating to some
	- Consulting - the team weighs in, but one person makes the final decision
		- \+ provides an opportunity for people to voice different opinions
		- \+ allows decision maker to collect information
		- \- it's frustating to those who opose the final decision
		- \- time-consuming
	- Consensus building - making a decision based on the majority perspective
		- \+ fair, equal say
		- \+ promotes collaboration
		- \- inefficient
- Use all the styles depending on the situation
### 2.1.4 Share information
- The main elements to consider
	- Forms
		- meetings
		- electronic
		- telephone
	- Frequency
		- how often? daily? as needed?
	- Boundaries
		- time off limits for messaging
### 2.1.5 Manage conflict
> Determine how your team will manage conflict before it arises
- Conflict management approaches
	- Accommodate - making room for another person's opinions
	- Compromise - making concessions to reach an agreement
	- Collaborate - working together to create a solution
- Ground rules for managing conflict
	- be understanding
	- refraining from gossiping
	- address conflict early
- Managing a conflict situation
	- acknowledge it
	- limit the conversation to the people involved
	- encourage communication after the conflict
	- negotiate a win-win scenario
### 2.1.6 Establish and maintain trust
> As a leader you should model the behaviour you'd like to see in your team
- Avoid say-do gap [me :(]
- Keep the lines of communication open
- Recognise your team members
- Make time for team building
- Find quick team wins to build confidence
## 2.2 Essentials for Strong Team Communication
### 2.2.1 Create a shared vision and focus on objectives
- ...
### 2.2.2 Provide feedback
- Ask permission to prepare the listener for the discussion
	- "Can I share something with you?"
	- "Is this a good time for feedback?"
- Use the Situation-Behaviour-Impact model
	- Situation - where and where the event took place
	- Behaviour - describe what you witnessed and avoid being judgemental
	- Impact - share the impact of the behaviour bad on you, or others who witnessed it
- Create a safe environment for giving/receiving feedback regularly
### 2.2.3 Encourage participation
- Collaborate on agendas
- Create a culture of participation
- Moderate team discussions - asking quiet team members to speak up
- Consider environmental factors
	- location, time of day, duration, group size
- Listen more than you speak
- Avoid distractions
### 2.2.4 Structure time for reflection
- After action review i.e. retrospectives lah
	- What happened
	- What worked well
	- What should we change
- Ground rules for after action review
	- Leave blames at the door
	- Leave rank at the door
	- Leave ego at the door
### 2.2.5 Hold teammates accountable
- Develop a responsibility matrix
	- Responsible - the person who actually completes the task
	- Accountable - the person who is held responsible by management 背锅佬
	- Consulted - the expert who provides information and opinions
	- Informed - the people aware of the decisions and actions
![](/source/images/RACI.png)
- Make contributions visible - social pressure serves as a reminder
- Give regular feedback
## 2.3 Modes of Team Communication
### 2.3.1 Communicating in face-to-face meetings
- Why are you holding the meeting?
	- Purpose: inform, decide, plan, brainstorm
- Who needs to be involved?
- What will be discussed during the meeting?
- When should the meeting take place?
- Where should the meeting take place?
### 2.3.2 Communicating via email
- Purpose - Why are you sending the email?
- Recipients - Who needs to receive the email?
- Content - Use specific subject line
### 2.3.3 Communicating on conference calls
- Initiate the call early (to avoid technical issues)
- Make sure those not speaking are muted
- Distribute the agenda beforehand
- Use signposts to keep everyone in the same place
- Address questions to a specific person
- Summarise and outline next steps
	- Action items and deadlines
### 2.3.4 Communicating in virtual meetings
- Consider visual aids
- Initiate the call early (to avoid technical issues)
- Consider your body language
- Look at the camera as much as possible
- Stand up when possible
- Factor in different time zones
### 2.3.5 Communicating across job functions
- Considerations about team members
	- Prior knowledge
	- Potential reactions
	- Perceived benefits
### 2.3.6 Communicating without words
- Body language
	- taking notes will signal to the group or speaker the information shared is important
	- make eye contact
	- nod appropriately
- Facial expression ^^
- Time management
- Seating
### 2.3.7 Communicating across cultures
- Language
	- provide materials in writing
	-  keep it simple
	-  don't defer to the most fluent speaker
- Nonverbal communication
	- assume positive intentions
	- give each other the benefit of the doubt